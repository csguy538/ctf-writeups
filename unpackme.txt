Here is the problem below:

Can you get the flag?
Reverse engineer this Python program.

After downloading the Python program, you will get a file name 
"unpackme.flag.py"

After running the program you get an output "What's the password?"

Not knowing the password, I went into the python script and a decrypt 
code on line 11 stating "plain = f.decrypt(payload)"

Knowing that plain is a decrypted text, I then used the print command to
print "plain."

b"\npw = input('What\\'s the password? ')\n\nif pw == 'batteryhorse':\n  print('picoCTF{175_chr157m45_616d21a3}')\nelse:\n  print('That password is incorrect.')\n\n"
What's the password? 

In that output You see the flag, "picoCTF{175_chr157m45_616d21a3}"
